package br.com.thiagogbferreira.microcalc.operation.first.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import br.com.thiagogbferreira.microcalc.operation.first.FirstOperationController;

/**
 * Execute the sum operation
 * @author axpira
 *
 */
public class FirstOperationTest {
  FirstOperationController app;

  @Before
  public void setup() {
    app = new FirstOperationController();
  }
  
  @Test
  public void runSomeSumAndValidateIfItIsOk() {
    assertThat(ResponseEntity.ok(new BigDecimal(5))).isEqualTo(app.getFirst(Arrays.asList(BigDecimal.valueOf(5), BigDecimal.valueOf(5))));
    assertThat(ResponseEntity.ok(new BigDecimal(10))).isEqualTo(app.getFirst(Arrays.asList(BigDecimal.valueOf(10))));
    assertThat(ResponseEntity.ok(new BigDecimal(1))).isEqualTo(app.getFirst(Arrays.asList(BigDecimal.ONE, BigDecimal.valueOf(2), BigDecimal.valueOf(3))));
    assertThat(ResponseEntity.ok(BigDecimal.ZERO)).isEqualTo(app.getFirst(Arrays.asList(BigDecimal.ZERO, BigDecimal.valueOf(0), new BigDecimal("0"))));
  }
  
  @Test
  public void needToReturnBadRequestWithEmptyList() {
    assertThat(ResponseEntity.badRequest().body(null)).isEqualTo(app.getFirst(Collections.emptyList()));
  }

  
}
