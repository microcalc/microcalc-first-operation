package br.com.thiagogbferreira.microcalc.operation.first;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Thiago Ferreira
 *
 */
@SpringBootApplication
@RestController
public class FirstOperationController {

  /**
   * @param args
   */
  public static void main(String[] args) {
    SpringApplication.run(FirstOperationController.class, args);
  }
  
  /**
   * Rest endpoint to sum all parameters
   * @param params to sum
   * @return sum of all params
   */
  @RequestMapping("/{params:.+}")
  public ResponseEntity<BigDecimal> getFirst(@PathVariable("params") List<BigDecimal> params) {
    if (params.isEmpty()) {
      return ResponseEntity.badRequest().body(null);
    }
    return ResponseEntity.ok(params.get(0));
  }
/*
  public void codigoSemTeste() {
    System.out.println("Teste");
    try {} catch (Exception e) {}
  }
*/
}
